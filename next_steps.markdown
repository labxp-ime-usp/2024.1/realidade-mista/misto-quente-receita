# Next Steps
Here, we propose some logical next steps. Of course, there is much more to be done, but we compiled this list of tasks we wanted to have done.

## Rendering Objects
After both cameras were working properly, we started using OpenGL to render a basic cube on the screen. The immediate following steps would be:

- Plotting both objects such that their perspective is properly adjusted for each eye perspective using the right sequence of transformations.
- Import/Convert `obj` files into OpenGL objects, rendering the first non-trivial 3D structure (e.g., a Pikachu)
- Implement plane detection and place the object. We recommend finding external libraries for this step.

## Improving the UI
The UI has two buttons: one for controlling spacing between both images and one for 90-degree rotation. We recommend the following:

- Encapsulate the control sliders/buttons into another menu to avoid cluttering (for example, a gear icon menu)
- Implement camera selection dropdown
- Implement switching both images (right to left / left to right)
- Rescale both images to fit the whole cellphone screen

## About the hardware
Sometimes the USB hub may fail to identify both cameras. We solved this problem by either properly checking the connections or keep trying to replug them until they started working.