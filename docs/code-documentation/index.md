---
layout: default
title: Code Documentation
nav_order: 2
has_children: true
---

This area of the documentation aims to describe the codebase in an abstract way
that is useful for understanding the purposes of the project and how we
implement it, and also from in a concrete manner explaining the actual code
structure, features, classes, etc.
