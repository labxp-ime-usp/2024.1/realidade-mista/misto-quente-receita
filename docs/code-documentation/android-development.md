---
layout: default
title: Android Development
parent: Code Documentation
nav_order: 2
---

# Android development basics

The AUSBC library which is used for the Misto Quente project builds over the core aspects of a usual 
Android application. Therefore, reviewing such concepts may be important to grasp the project implementation.

## Android Activities

Activities are one of the major components of Android applications. They are Java/Kotlin abstractions
that compose the program. An Android code consists of the interactions of different Activites.

One example of how this more Object-oriented paradigm differs from usual code can be seen in the way 
the Android applications are started. Instead of defining a main function that starts the entire program,
we would rather define the Main Activity that starts with the application. Then we override its onCreate()
method with the commands that get executed alongside the application. The rest of the software execution
flow is then just new Activities being instantiated, interacting with each other, executing specific
services, etc.

An Activity has a lifecycle: it is first created (triggering its callback function onCreate()).
It can also be paused (which triggers the onPause()) or destroyed (that triggers the onDestroy() method).
There are other possible actions, such as resuming. More information can be seen in the official documentation.

## Android Fragments

Android Activities which handle many different aspects of the application behaviors can be modularized
with fragments. This enables one to implement a different Fragment for each functionality of said Activity,
resulting in a more modular code organization, which allows Fragments to be imported and invoked concisely. 

This feature is heavily used by the AUSBC library.

## Android Views and Layouts

The Android development framework offers views (also known as widgets) to enable developers to display content 
on the screen. The layouts are used to determine how the different views can be disposed of on screen.
It is possible to bind the defined view in a xml layout file using [View Bindings](https://developer.android.com/topic/libraries/view-binding).
