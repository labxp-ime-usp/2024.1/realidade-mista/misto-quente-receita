---
layout: default
title: General Structure
parent: Code Documentation
nav_order: 2
---

# General Description

The _Misto Quente_ project is implemented in the native Android platform using
the standard Kotlin project structure, which is practically identical to the
Java project structure. It uses Gradle as its build automation tool.

The core dependency of the project is the API Android Camera2, which is the
standard library (represented by the `android.hardware.camera2` package) for
plug-and-play USB cameras (webcams) in the Android platform.

# Base Used

Because the project aims to provide a lightweight AR application using two
general external USB plug-and-play cameras (webcams), _Misto Quente_ uses the
[official Android sample project for Video][camera2-samples-repository] (using
the `android.hardware.camera2` package) as its base, serving as a template.
Out-of-box, the sample project provides:

- Recognition and choosing of connected external USB cameras
- Setting resolution and framerate
- Capturing from two sources


<!--- References --->
[camera2-samples-repository]: https://github.com/android/camera-samples
