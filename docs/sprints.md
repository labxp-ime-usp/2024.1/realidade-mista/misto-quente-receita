---
layout: default
title: Sprints
nav_order: 2
---

This area of the documentation aims to register the Sprints.

# Sprint 0

This is an initial sprint focused on collecting information regarding the previous group project, XR and Unity's XR frameworks.
Not a serious development sprint, but an effort to prepare the group as fast as possible to the project and elaborate a more
accurate goal for next week sprint.

# Sprint 1

The true first sprint. It is focused in developing the initial UVC camera detection and display for the Misto Quente application.
It intended to last 2 weeks and contained the following objectives:
+ Learn how to read from an external camera input.
+ Create screen abstraction.
+ Create panels abstractions.
+ Reproduce an external camera input in a screen with one panel .
+ Reproduce two external camera inputs in a screen with two panels.

# Sprint 2

In **Sprint 1**, we succeeded in reproducing the two external camera inputs, and
found a good free software library for it. In this sprint, the goal is to use
this library to develop the base _Misto Quente_ Android native application.

### Deliverables

+ Detect and select external cameras.
+ Display external cameras output on the smartphone screen, side-by-side.
+ Allow for the video stream calibration (streams separation, rotation, etc.).

# Sprint 3

In **Sprint 2**, we evolved the base for the _Misto Quente_ Android native
application, and got a better grasp of the UI components involved. The
deliverables of **Sprint 2** weren't reached, so they are technically the same
(although the ground for reaching them was nicely laid out in this sprint). In
contrast, the direction to take is clearer and more detailed.

### Deliverables

+ Detect and select external cameras.
+ Display external cameras output on the smartphone screen, side-by-side.
+ Allow for the video stream calibration (streams separation, rotation, etc.).

### Detailed Assignments

##### **Marcelo & David**: AUSBC Logic

+ Port the logic of AUSBC to the current _Misto Quente_ project.

##### **Vitor & Arthur**: Core UI

+ Implement a starting activity that comes before the core activity (streams
of the two cameras) to serve as an entry menu for the application.
+ Implement a button that will be placed at the top center of the screen in the
core activity, between the two camera streams, that will be used for
calibration.
+ Implement an activity for the calibration menu that will show when the
calibration button is pushed.
