---
layout: default
title: Meeting logs
nav_order: 2
has_children: true
---

This area of the documentation aims to register the main points discussed on every weekly meeting.

