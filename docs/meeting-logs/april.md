---
layout: default
title: April
parent: Meeting logs
nav_order: 1
---

# April 1

The prototype of the Misto Quente Documentation repository was implemented as a GitLab Wiki. 

Also, the original Misto Quente repository from the previous team that worked with it was migrated to our new repository.

# April 5

The client presented to the team his prototypal devices for XR. The group was given the pair of connected cameras which were meant to 
enable the requested MR experience.

A showdown of the actual state of the previous state and its initial calibration scene was done. It clarified the group's insight over
the client's intention.

Some members of the group had problems using UnityHub and opening the project in a Linux setup.

# April 8

A pilot "studying" sprint was started. More details in the Sprints section.

# April 11

The group studied together the main AR frameworks available and how they could be integrated with Unity interface and used 
in an Android application.

# April 15

The group looked for ways to use a single camera setup for the project and studied ways to emulate stereoscopy with it. 
In the end, the idea has been discarded, and it was decided to stick with the original dual camera setup.

# April 18

The idea of leaving Unity framework has been strongly discussed. The group considered making its own AR application from scratch. 
It was estimated that it would require the implementation of an Android application that could read both UVC cameras and display their images
on screen. Then, the group should also study plane detection and tracking algorithms to be implemented and interact with the cameras input.
Finally, the capability of displaying AR objects on screen, over detected planes on both images in such a way it creates a stereoscopic effect.
This would solve the semestral objective proposed by the client, but the team should be aware that capabilities of tracking, interaction and
opening custom AR scenarios should be implementable in the future.

The team talked with the client about it, and he has shown aprehension. He thinks that developing Misto Quente in Unity would make 
custom AR scenarios made in Unity portable in it.

The group decided to discuss this possibility with caution, and reflect more over the client's intentions and available outcomes.

# April 22

The group decided to develop the Android application from scratch, and give up on Unity Game Engine. Then, discussed the necessary steps and abstractions to 
implement such program. The following was decided:

+ The application needs to read connected UVC cameras and display them on the Android device as a stereoscopic image pair. 
+ The application needs to evaluate plane detection from the given UVC input.
+ The application needs to display a simple graphical object on screen, based on given camera input and plane detection.

At the ending of the meeting, the group started the first development sprint (Sprint 1).

# April 25

The group prepared an initial Android application template from Android Studio and tried to push it to the new GitLab repository.
The members also discussed with professor Alfredo Goldman and Joseph Yoder about the recent decisions. Then another meeting with 
the client happenned, and he was more convinced about the group's ideas.

