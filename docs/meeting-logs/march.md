---
layout: default
title: March
parent: Meeting logs
nav_order: 3
---

# March 18

The team members met each other for the first time.

# March 21

The team members talked with the client for the first time.

General context was given about the project and its previous development state. A basic Unity scene was already developed for calibrating cameras input.
There were however, two core problems to be solved:

1. The actual processing occurs in a computer running the Unity application. It is desirable to allow the android device to handle this by itself.
2. The implementation of a way for the android device to handle the visual input from the cameras and let it be processed by the Unity application.

It was also proposed to divide the team in 2 fronts:

1. Two people handling the development of the Unity application, adding the software features.
2. Two people studying the integration of the Unity application with the external camera devices.


