---
layout: default
title: May
parent: Meeting logs
nav_order: 4
---

# May 1

The group had a single meeting during this week.

The group decided to invest the week in the individual development of the basic interaction of the android
device and camera. The member Marcelo took the camera devices to test possible prototypes during the weekend.
The other members were assigned to study the problem and test possible implementations, while also developing their prototypes
to be tested by Marcelo.

Victor shared with the group resources regarding the available Android APIs for handling USB devices.

The group also decided that next week the task of documenting the project would be assigned
for a different member each week.

# May 6

The group started the first meeting of the week discussing the material that
Vitor brought in the last meeting about the [API Android Camera2][api-android-camera2] which is the
standard library (represented by the `android.hardware.camera2` package) for
plug-and-play USB cameras (webcams) in the Android platform.

Then the group experimented with [official samples repository for the
library][camera2-samples-repository], and found the example project
`Camera2Video`. These are the some of the capabilities listed on the sample
project README:

1. Change the dimensions and frame rate of the video
2. Choose available cameras
3. Capture using two streams directly to a SurfaceView, or one stream to an EGL
pipeline

Because of this, the group opted to use the sample project (which is licensed
under the Apache 2.0 license, so a permissive one) as the base for the
development of _Misto Quente_.

Finally, the group had a retrospective with Professor Alfredo and lifted some
problems and solutions on the groups's workflow.


# May 9

The group started the meeting trying to passthrough the USB port to the emulator
to test the use of plug-and-play USB cameras. Infortunately, this was
unsuccessful, which prompted the group to deploy and test it in a real Android
device.

The effort was unsuccesful still, and the group theorized (based on the code)
that the reason was because the framerate of the webcams used had specs that
weren't high enough for the implementation. In turn, the group found another
repository reference named [Android USB Camera][AUSBC], which when deployed to a
real Android device validated that we could stream two webcams (the same that
failed in the previous attempt) to a native Android application.

The group finished the meeting recording a video that summarized the progress
made in the last two weeks and the important milestone hit. The group sent this
video to the client.


# May 13

The group started discussing about how to maintain the documentation.

The main focus of the meeting was to study the [Android USB Camera][AUSBC]
application to understand how to use the lib to stream two simultaneous webcams.

Before the end of the meeting, Prof. Alfredo introduced the group to two other
professors. The group explained the overall _Misto Quente_ project and talked
about the state of things, challenges, and how to move forward. The professors
gave some feedback about those topics.


# May 16

The group started the meeting discussing the architecture of the demo
application of AUSBC provided by the developer.  This demo application uses
AUSBC as a library, which is an option on implementing _Misto Quente_, as all
the complexity of handling the actual plug-n-play USB cameras will be abstracted
by its use.

The group studied together the flow of execution of the demo application, which
has its entrypoint in an activity that inherits from the base class
_androidx.appcompat.app.AppCompatActivity_.

From the second part of the meeting onward, the group had an online conference
call with the client.  The group reported and explained the last developments of
having a base to implement _Misto Quente_, a demo that validated the simultaneos
videostream of the two "generic" plug-n-play cameras provided by the client, and
the possible use of the fully implemented project as a middleware for other
mixed reality applications.


# May 20

The group started the meeting debating the direction to take, about what the
priorities are. The conclusion was that the group could start segmenting the
work as the UI and the actual "wiring" of the external cameras could advance in
parallel.

The group then dedicated about 15 minutes to review the integrants understanting
of core concepts of Android applications, like `Activities` and `Fragments`,
`Views` and `Layouts`, bindings, and more.

The group used the rest of the meeting to mob program and start building the UI
for the project. The aim was to produce a simple `LinearLayout` that partitions
the screen into two, which will be feeded with each of the two USB external
camera streams.


# May 23

The group started by looking into the progress made in the UI front that began
at the latter half of the last meeting. The group successfully created the
`LinearLayout` for proposed, with a validation of the back internal camera feed
being streamed into one of the partitions.

The group then focused on trying to stream the external USB cameras using the
`CameraFilter` interface, which is the suggestion by the package mantainers to
handle external USB cameras. However, the attempts were unsuccessful.

The group then started organizing the next sprint and assigning tasks for each
of the groups pairs. The overall goals of the sprint are a continuation of the
unsuccessful ones of the last sprint, but with a better detailment of the tasks
and the scope.


# May 27

The part of the group focused on the external USB cameras integration started
the implementation of a simple Android Activity to display the image captured by
the webcams in the screen. However, the code couldn't be tested, since the aUSBc library
has a compatibility issue with the version 14 of Android, causing the application to crash. They spent the rest of the week trying to fix this issue.

The other part of the group continued on their work of improve the UI of the application. They made some
improvements on legibility and consistency of the code of the main screen, which display two sections correspondent to each one of the webcams display. After that, they started to implement some calibration features.


<!--- References --->
[api-android-camera2]: https://developer.android.com/reference/android/hardware/camera2/package-summary.html
[camera2-samples-repository]: https://github.com/android/camera-samples
[AUSBC]: https://github.com/jiangdongguo/AndroidUSBCamera
