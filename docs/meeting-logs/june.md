---
layout: default
title: June
parent: Meeting logs
nav_order: 5
---

# June 3

The part of the group focused on the external USB cameras integration continued their investigation about the compatibility issues with the version 14 of Android. They found an fork of the original libausbc with had the promise to resolve this issue, and started to adapt our code to embrace these modifications.

The other part of the group, focused on the UI, made progress in their work, displaying the images in the way that they pretended. After this, they started to do the the "calibration" part of the UI, allowing the user to adjust where the images are displayed (horizontally) and how they are displayed (starting some work with rotation).

# June 10

The pair Marcelo and David was responsible for integrated the "downstream"
version of the AUSBC compatible with Android 14. During the week, they finally
found a way to build the patched AUSBC version from source using it as a
subproject, and in the reunion they paired to solve problems related to the
build of `libuvc`, which the upstream authors made dependent on a specific
Android Native Development. Weirdly enough, David's NDK was broken while
Marcelo's was fine, despite David reinstalling the exact NDK from Marcelo.

The pair Arthur and Victor focused on finishing the UI elements and also took
the opportunity to lay the ground for the integration of both pairs work. 

# June 14

A good part of the reunion was focused on discussing with Professor Goldman how
to leverage the pros of opting to go with an Android native approach to
aggregate value to _Misto Quente_ for the client, which was adamant on the use
of Unity. We came to the conclusion that, despite the switch was unavoidable
(there is no support for external cameras through the ARCore plugin used by
Unity to build Android XR apps), we must understand that this doesn't have
direct value to the client by itself, so we need to think from his perspective
to also understand how this necessary decision is more beneficial than at face
value.

The rest of the meeting was spent a mob session to integrate both pairs work,
i.e., the integration of the working prototype for the external multi-camera
stream (that used the downstream patched AUSBC lib) with the UI development of
the layouts and views. This was unsuccessful.

# June 17

The meeting as a continuation of last meeting, in which the integration of both
pairs work was attempted (unsuccessfully). The group made a mob to debug why the
app was crashing at launch, and discovered that it was related to an
uninitialized view that happened because the constructor of the mother class,
`super()`, was called too early.

Then the group struggled with stabilizing the double stream of the external
cameras, until they concluded that the instability was possibly due to the USB
hub used, as leaving the hub untouched after getting the double stream to work
stabilized things.  

There was a split in pairs at the end (Marcelo and Arthur, David and Victor) to
solve a problem with the rotation feature of the UI, and to elaborate on a
report to the client along with sending an updated APK.

# June 20

After successfully reaching the milestone of a stable double stream of the
external USB cameras, the group focused the whole meeting to:

1. Discuss and educate the group about the OpenGL features the group aims to
implement;
2. Mob-programming to assess and start understanding the Android
JetPack native Views for OpenGL and the AUSBC implemented View for OpenGL.

Arthur and Marcelo have a better understanding and computer graphics, so a great
deal of the meeting dedicated to educate the other members (David and Victor)
about the core and essential concepts of shaders, matrices, and transformations,
which comprises the above bullet point number 1.

Because using Views with capabilities of rendering OpenGL objects is a common
task on Android development, many official and "upstream" View implementations
to handle this problem. However, AUSBC also has an implementation of a View for
this task that functions like a wrapper to the aforementioned one. The rest of
the meeting was dedicated to mob-programming to investigate these
implementations, and understand where we least needed to adapt for our domain,
which comprises the above bullet point number 2.

# June 23

Most of the meeting was spent on continuing the effort described on the bullet
point number 2 of last meeting, i.e., mob-programming to tackle the OpenGL
implementation for rendering arbitrary shapes and assets in 3D.

It is worth noting that the group also talked about how two different mirrored
shaders would need to be implemented to preserve the stereoscopic illusion of
seeing this OpenGL object "inside the mixed reality".

At the very end, the group devised some time to organize the presentation for
Thursday.
