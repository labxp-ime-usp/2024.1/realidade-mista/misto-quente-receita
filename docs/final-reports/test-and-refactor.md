---
layout: default
title: Test/Refactor
parent: Final Reports
nav_order: 2
---

At July 4, the group reunited at an online meeting to do the test and refactor day tasks. 
The group started with the refactoring task. The refactoring activities, in their execution order,
were the following:  
+ Merging the OpenGL branch, with most recent code, to the master branch.
+ Setting some variables to private.
    + connectedCameras
    + WIDTH_PERCENT
+ Grouping lines of code into new helper functions.
    + setSeekBarListeners().
+ Setting id of programatically created views.
+ Uncommenting methods to adjust margins of camera images.
+ Styling changes.
    + Reordering and formatting methods.
+ Removing commented layout draft components.
+ And other minor modifications to organize how code is displayed.

After finishing the refactoring, the group discussed about how to proceed with the implementation
of the tests. An agreement was made about the fact that implementing tests for this project was not viable
for the current context. Implementing tests, would require a needlessly high amount of complexity for this
specific project, such as mocking devices, interacting with the external lib, etc.

Because of that, the group took extra care at doing the refactoring, focusing more at it and incrementing
the amount of refactored code.
