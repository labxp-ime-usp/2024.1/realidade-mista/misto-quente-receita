---
layout: default
title: Overview of Project Mature Phase
parent: Final Reports
nav_order: 2
---

This page is destined to explain what was done to fulfill the itens below that
comprise the mature phase of the project: 

| Item | Explanation |
| -------- | ------- |
| Code | The codebase was integrated with the main lib that allowed us to stream two external USB cameras and was stabilized. |
| Tracking | Mainly, the tracking was done by keeping meeting logs, sprints documentation, consistent stand-up meetings, and extra-hours reports. |
| Automated Tests | Because of the peculiarity of the project (the core of the domain and the app is device management) we both couldn't find any worthy units to test, neither found a clean way to mock devices in a testable way. |
| Test Coverage | Because of the peculiarity of the project (the core of the domain and the app is device management) we both couldn't find any worthy units to test, neither found a clean way to mock devices in a testable way. |
| CI/CD Pipeline | Just like with testing, because of the peculiarity of the project, there wasn't a clear and worthy way to continuously integrate software, while there was no sense in deploying the developed app. |
| Team | Because of the many roadblocks we faced, the group grew closer and with great chemistry. We hardly ever missed any meetings and used a lot of mob-programming to take advantage of the time together, while also having great off-meeting communication. It also notable that each team member had its expertise and particular set of skills (communicability, hard-skills, team management, pro-activeness, etc.) which made for a really well-balanced and complete team, although it was one of the smallest groups. |
| Test-Driven-Development | Because of the peculiarity of the project (the core of the domain and the app is device management) we both couldn't find any worthy units to test, neither found a clean way to mock devices in a testable way. |
| Invisible Coach | Each member of the group took ownership of responsabilities and of maintaining high team morale and productivity. Most of the "coach responsabilities" (at least, the ones perceived by us as coach-specific responsabilities) were organically rotated through the whole group, for example: keeping meeting-logs, elaborating the status of the project and communicating group decisions to the client, deciding next steps and allocation of pairs for segmented work, explaining concepts and assessing team capabilities, and else. |
| Project | Considering that the legacy project inherited by us didn't have a built application to the Android platform (it only worked on the _Play-in-Editor_ mode, i.e., running the scene directly in the Unity IDE), and was a dead-end (because of the Unity constraint of only having support for in-built cameras on Android), as we ended the semester with a working prototype for streaming (simultaneously) two external USB cameras with calibration and OpenGL capabilities, and running natively on Android (both with low-end and high-end specs), we can consider that the project as a whole was a success. It is important to notice that the main deliverable from the client was to port the application to native Android. |
| Other deliverables | It is true that we took a long time to produce deliverables, and that they weren't frequent, but we always showed progress in terms of assessing the roots of the many roadblocks encountered throughout the semester and taking hard and assertive decisions to solve those, while always communicating it to the client. |
