---
layout: default
title: Final Reports
nav_order: 2
has_children: true
---

This area of the documentation aims to report the final observations of the group development process.
Things such as refactor/test day reports, members self-evaluation, etc, can be found here.
