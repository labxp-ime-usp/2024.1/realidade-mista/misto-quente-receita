---
layout: default
title: Team Organization
nav_order: 2
has_children: false
---

# About the team

## General organization

The development team consists of four people, with a couch and a tracker. All members have weekly meetings every monday 4 PM and
thursday 4 PM, each meeting having 2 hours of length. Each member also dedicates 4 individual hours to the project. This counts up to 8
hours dedicated to the project per person.

## Agile methods

There is a kanban that helps to organize development sprints (https://trello.com/b/HCVHWB5R/labxp-mac0472). 



