# Misto Quente Receita

## About the repository
This is the official documentation repository for the Misto Quente Project (https://gitlab.com/labxp-ime-usp/2024.1/realidade-mista/Misto-Quente).

## GitLab Pages for the documentation
The documentation is intended to be read on its GitLab Pages website. Go to https://labxp-ime-usp.gitlab.io/2024.1/realidade-mista/misto-quente-receita to access it.
