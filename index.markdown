---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---

# Important information

+ [Old repository copied from previous group's work](https://gitlab.com/labxp-ime-usp/2024.1/realidade-mista/Misto-Quente).
+ [Official repository](https://gitlab.com/labxp-ime-usp/2024.1/realidade-mista/mistoquente).
+ [Kan Ban](https://trello.com/b/HCVHWB5R/labxp-mac0472) (May request permission to access).
+ [Extra hours control sheet](https://docs.google.com/spreadsheets/d/1Uj-IIX1xfnAXBkE2qycG4QSgNFoLSGGLRiufiXgg6IE/edit#gid=0).
