---
layout: page
title: About
permalink: /about/
---
Augmented Reality (AR) and Virtual Reality (VR) are technologies that bring more immersive virtual experiences. While the former implements the 
addition of virtual elements in a real environment, the latter inserts the user in a isolated completely virtual world. Mixed Reality appears in this
context as mix between both, using concepts from both to create a half-virtual, half-real experience. XR represents the entire AR-VR spectrum.

Misto Quente is a project that aims to make Mixed Reality more accessible to general public. Its core idea is to enable users to by cheaper hardware resources by 
themselves, and integrate it with Misto Quente application to get a custom home-made MR device (hence the name "Misto Quente", alluding to a food that can be easily made
and incremented depending on one's range of available ingredients). 

This can be accomplished by developing an android Unity application that receives visual input (it could be from a pair of webcams that simulate eyes), and 
generates MR output, which consists of the collected visual input modified with AR elements and divided into two "lens". This output being displayed on a 
cellphone screen could then be used to improvise the mobile device as XR googles, enabling easy MR experience without the need to buy an expensive dedicated device.

The official repository is https://gitlab.com/labxp-ime-usp/2024.1/realidade-mista/mistoquente
